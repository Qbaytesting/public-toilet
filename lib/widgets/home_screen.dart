import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:signalstrength/models.dart';
//import 'package:flutter_rating_bar/flutter_rating_bar';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import './add_new_washroom_details.dart';
import './feedback.dart';
import './ListWidget.dart';


class GoogleMapView extends StatefulWidget {
  @override
  GoogleMapState createState() => GoogleMapState();
}

class GoogleMapState extends State<GoogleMapView> {
  BuildContext context;
  GoogleMapController mapController;
  Set<Marker> _markers;

  LatLng _center = LatLng(45.521563, -122.677433);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    _markers = Set();
    moveToUserCurrentLocation();
  }

  void moveToUserCurrentLocation() async {
    debugPrint("move to current location");
    GeolocationStatus geolocationStatus  = await Geolocator().checkGeolocationPermissionStatus();
    debugPrint("data ${geolocationStatus}");
    if(geolocationStatus ==GeolocationStatus.unknown)
    {
      return;
    }
    else if (geolocationStatus == GeolocationStatus.denied) 
    {
      return;
    }
    else if (geolocationStatus == GeolocationStatus.disabled)
    {
      return;
    } 
  // Take user to location page
    else if (geolocationStatus == GeolocationStatus.restricted) 
    {
      return;
    }
    else if (geolocationStatus == GeolocationStatus.granted) 
    {
      return;
    }

    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    debugPrint("position ${position.latitude}");
    //Future<NearByWashRoomList> washRoomDetails=fetchPost(position.latitude,position.longitude);

    setState(() {
      debugPrint("move to current location state ${position.latitude}");

      LatLng currentLatLng = LatLng(position.latitude, position.longitude);
      mapController.moveCamera(CameraUpdate.newLatLngZoom(currentLatLng, 15));
      _markers.add(Marker(
          markerId: MarkerId(position.toString()),
          position: currentLatLng,
          icon: BitmapDescriptor.defaultMarker));
    });
  }

  void _onMapClicked(LatLng latLng) {
    //moveToUserCurrentLocation();
    setState(() {
      if (_markers != null) {
        _markers.add(Marker(
            markerId: MarkerId(latLng.toString()),
            position: latLng,
            icon: BitmapDescriptor.defaultMarker));
//_showInfoDialog(this.context);
      }
    });
    debugPrint("total markers ${_markers.length}");
  }

  @override
  void initState() {
    Future<bool>.microtask(() => Geolocator().isLocationServiceEnabled())
        .then((bool isLocationEnable) {
      if (isLocationEnable) {
        fetchPost().then((NearByWashRoomList washRoomLists) {
          for (int i = 0; i < washRoomLists.washRoomDetails.length; i++) {
            _markers.add(Marker(
                markerId: MarkerId(LatLng(
                        double.parse(
                            washRoomLists.washRoomDetails[i].washRoomLat),
                        double.parse(
                            washRoomLists.washRoomDetails[i].washRoomLng))
                    .toString()),
                position: LatLng(
                    double.parse(washRoomLists.washRoomDetails[i].washRoomLat),
                    double.parse(washRoomLists.washRoomDetails[i].washRoomLng)),
                icon: BitmapDescriptor.defaultMarker,
                onTap: () {
                  debugPrint("marker clicked");

                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: new Text("Toilet Info"),
                          content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Distance : ${double.parse(washRoomLists.washRoomDetails[i].distance).toStringAsFixed(2)}",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Text("Rating : "),
                                    FlutterRatingBar(
                                        initialRating: double.parse(
                                            washRoomLists
                                                .washRoomDetails[i].rating),
                                        fillColor: Colors.amber,
                                        borderColor: Colors.amber.withAlpha(50),
                                        allowHalfRating: false,
                                        onRatingUpdate: null,
                                        itemSize: 19)
                                  ],
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Place : ${washRoomLists.washRoomDetails[i].place}",
                                    style: TextStyle(fontSize: 17),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Gender : ${washRoomLists.washRoomDetails[i].genders}",
                                    style: TextStyle(fontSize: 17),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Hygienic : ${washRoomLists.washRoomDetails[i].higenic}",
                                    style: TextStyle(fontSize: 17),
                                  ),
                                )
                              ]),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(
                                "Close",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        );
                      });
                }));
          }

          setState(() {
            debugPrint("set state from initState ${_markers.length}");
            this._markers = _markers;
          });
        });
      }
      else
      {
        showDialog(
          context: context,
          builder: (BuildContext buildContext)
          {
              return AlertDialog(
                title: Text("Caution"),
                content: Text("Please ensure your location service is enabled and permission granted"),
              actions: <Widget>[
                FlatButton(child: Text("Okay"),
                onPressed: ()
                {
                  Navigator.of(context).pop();
                },
                )
              ],);
          }
        );
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    // _markers=Set();

    //Future<bool> isLocationEnabled = Geolocator().isLocationServiceEnabled();

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Near by Toilets"),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddNewPlaceDetails()),
                );
              },
              child: Image.asset('assets/add.png'),
            )
          ],
        ),
        body: Container(
          child: GoogleMap(
            onMapCreated: _onMapCreated,
            initialCameraPosition: CameraPosition(target: _center, zoom: 11.0),
            onTap: _onMapClicked,
            markers: _markers,
            onLongPress: _onMapClicked,
          ),
        ),
        drawer: Drawer(
          child: ListView(
    children: <Widget>[
      Image.asset('assets/qbaylogo.png',width:50,height:50),
      ListTile(
       
       
      ),
      
      ListTile(
        title: Text("List"),
        trailing: Icon(Icons.arrow_forward),
        onTap: (){
          Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => WashRoomListWidget()),
                );
        },
      ),
      ListTile(
        
        title: Text("Feedback"),
        
        trailing: Icon(Icons.arrow_forward),
        onTap: (){
          Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FeedbackWidget()),
                );
        },
      ),
    ],
  )
        ),
      ),
    );
  }
}

VoidCallback _showInfoDialog(@required BuildContext context) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Location Info"),
          content: new Text("data"),
          actions: <Widget>[
            FlatButton(
              child: Text("close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      });
  return null;
}

Future<NearByWashRoomList> fetchPost() async {
  Position position = await Geolocator()
      .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

  Map<String, String> bodyData = Map();
  bodyData["Latitude"] = position.latitude.toString();
  bodyData["Longitude"] = position.longitude.toString();

  final response = await http.post(
      'http://quadrobay.co.in/Toilet_App/public/api/showroom/toilet-get-data',
      headers: {"Content-Type": "application/json"},
      body: json.encode(bodyData));

  debugPrint('body data ${bodyData.toString()}');
  if (response.statusCode == 200) {
    debugPrint("data loaded with 200Ok ${response.body}");
    //NearByWashRoomList washRoomDetails = NearByWashRoomList.fromJson(jsonDecode(response.body));
    return NearByWashRoomList.fromJson(jsonDecode(response.body));
  } else {
    debugPrint("failed to load");
    throw Exception('Failed to load post');
  }
}
