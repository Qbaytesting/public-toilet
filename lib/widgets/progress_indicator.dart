import 'package:flutter/material.dart';

class ProgressDialogWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: CircularProgressIndicator(),
    );
  }
}
