import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:camera/camera.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:uuid/uuid.dart';

class FeedbackWidget extends StatefulWidget {
  FeedbackWidgetState createState() => FeedbackWidgetState();
}

class FeedbackWidgetState extends State<FeedbackWidget> {
  String errorSummary, screen, suggestion, deviceID;
  File feedbackScreenImage;
  TextEditingController _errorSummaryController = TextEditingController();
  TextEditingController _suggestionController = TextEditingController();

  @override
  Widget build(BuildContext context) {


    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(title: Text("Feedback"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: (){Navigator.pop(context,true);}
        )),
        body:Builder(
          builder: (BuildContext context)
          {
            return  Container(
          child: Column(
            children: <Widget>[
              Text("Select Screen:"),
              DropdownButton<String>(
                items: [
                  "Home",
                  "Toilet List",
                  "Add Toilet",
                  "Toilet Info",
                  "Others"
                ].map((String value) {
                  return new DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                  );
                }).toList(),
                value: screen,
                onChanged: (selectedData) {
                  setState(() {
                    screen = selectedData;
                  });
                  debugPrint("${selectedData} selected data");
                },
                hint: Text("Select Screen"),
              ),
              Text("Error Summary"),
              TextField(
                controller:_errorSummaryController
              ),
              Text("Images (If Any)"),
              FlatButton(
                child: isImageAvailable(),
                onPressed: () {
                  getImage();
                },
              ),
              Text("Suggestions"),
              TextField(
                controller: _suggestionController,
              ),
              RaisedButton(
                child: Text("Send Feedback"),
                onPressed: () {
                  sendFeedbackToServer(context);
                  debugPrint("Feedback sent successfully");
                },
              )
            ],
          ),
        );
          },

        ),
      ),
    );
  }

  

  void sendFeedbackToServer(BuildContext context) async {
    String date = new DateTime.now().toString();
    debugPrint(
        "datetime ${date} summary ${errorSummary} suggestion ${suggestion} Screen ${screen}");

    // Map<String,String> data = new Map();
    // data["Summary"]=errorSummary;
    // data["Suggestion"]=suggestion;
    // data["Screen"]=screen;
    // data["Date"]=date;
    // http.post("http://quadrobay.co.in/Toilet_App/public/api/showroom/toilet-add-feedback",
    // headers: {"Content-Type": "application/x-www-form-urlencoded"},body:data,encoding: Encoding.getByName('utf-8')).then((response){
    // debugPrint("Submit response ${response.body}");
    // Navigator.of(context).pop();
    // });

    // multipart request
    var uri = Uri.parse(
        "http://quadrobay.co.in/Toilet_App/public/api/showroom/toilet-add-feedback");
    var request = new http.MultipartRequest("POST", uri);
    var uuid = new Uuid();
    var imageName = uuid.v4();
    ;
    var multipartFile = http.MultipartFile.fromBytes(
        "pic ${imageName}.png", feedbackScreenImage.readAsBytesSync());
    request.files.add(multipartFile);
    request.fields["Summary"] = errorSummary;
    request.fields["Suggestion"] = suggestion;
    request.fields["Screen"] = screen;
    request.fields["Date"] = date;

    var response = await request.send();

    if (response.statusCode == 200) {
      debugPrint("feedback response is ${response}");
      final snackBar = SnackBar(content: Text('Feedback sent successfully'));
      Scaffold.of(context).showSnackBar(snackBar);
      setState(() {
       feedbackScreenImage=null; 
       errorSummary="";
       _errorSummaryController.text="";
       _suggestionController.text="";
       suggestion="";
       screen=null;
       date="";
      });
      //Navigator.of(context).pop();
    } else {
      final snackBar = SnackBar(content: Text('Sorry. Something went wrong'));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      //   debugPrint("captured image details ${image} @ position ${position}");
      feedbackScreenImage = image;
    });
  }

  Widget isImageAvailable() {
    debugPrint("check for image availability");
    if (feedbackScreenImage == null) {
      debugPrint('image not available');
      return Text('No image');
    } else {
      // debugPrint('image available @${position}');
      return Image.file(feedbackScreenImage,
          fit: BoxFit.fill, height: 50, width: 50);
    }
  }
}
