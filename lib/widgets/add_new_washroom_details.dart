import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'package:camera/camera.dart';
import 'package:signalstrength/widgets/camera_widget.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dialog.dart';

import 'package:signalstrength/widgets/dialog.dart';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


// class test extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<test> {
//   GoogleMapController mapController;

//   final LatLng _center = const LatLng(45.521563, -122.677433);

//   void _onMapCreated(GoogleMapController controller) {
//     mapController = controller;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text('Maps Sample App'),
//           backgroundColor: Colors.green[700],
//         ),
//         body: GoogleMap(
//           onMapCreated: _onMapCreated,
//           initialCameraPosition: CameraPosition(
//             target: _center,
//             zoom: 11.0,
//           ),
//         ),
//       ),
//     );
//   }
// }



class AddNewPlaceDetails extends StatefulWidget {
  @override
  AddNewPlaceDetailsState createState() => AddNewPlaceDetailsState();
}
 
class AddNewPlaceDetailsState extends State<AddNewPlaceDetails> {

double latitude=0.0,longitude=0.0;
  String _placeAddress = "";
  List<String> genderList = ['Male', 'Female', 'Both'];
  List<String> hygineList = ['Clean', 'Good', 'Dirty'];
  List<String> placesList = [
    'Restaurant',
    'Shopping Center',
    'Public',
    'Gas Station'
  ];
  List<String> paymentDetails = ['Free', 'Pay'];
  List<String> disabledAccess = ['Yes', 'No'];
  String currentGender = 'Male',
      currentHygineStatus = 'Clean',
      places = 'Restaurant',
      currentPayment = "Free",
      currentDisabledAccess = 'Yes';
  List<DropdownMenuItem<String>> genderDropDown,
      hygineDropDown,
      placesDropDown,
      paymentDropDown,
      disableAccessDropDown;
  CameraController _controller;
  Future<void> _initializeControllerFuture;

  List<File> _image = new List(3);

  Future getImage(int position) async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
  
    setState(() {
      debugPrint("captured image details ${image} @ position ${position}");
      _image[position] = image;
    });
  }

  @override
  void initState() {
    debugPrint('washroom widget inits');
    genderDropDown = getGenderDropDownMenuItems(genderList);
    hygineDropDown = getGenderDropDownMenuItems(hygineList);
    placesDropDown = getGenderDropDownMenuItems(placesList);
    paymentDropDown = getGenderDropDownMenuItems(paymentDetails);
    disableAccessDropDown = getGenderDropDownMenuItems(disabledAccess);

    super.initState();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    if(_controller != null)
    {
        _controller.dispose();
    }
    // _controller.dispose();
    super.dispose();
  }

  Set<Marker> _markers;
  GoogleMapController mapController;
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    _markers = Set();
    moveToUserCurrentLocation();
  }

  List<DropdownMenuItem<String>> getGenderDropDownMenuItems(List<String> data) {
    List<DropdownMenuItem<String>> items = new List();
    for (String d in data) {
      items.add(new DropdownMenuItem(value: d, child: new Text(d)));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    debugPrint(
        "gender list ${genderDropDown.length} and current gender ${currentGender} ${(_image[0]==null?"no img":"Img avail")}");
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text("Add New Details"),
          leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: (){Navigator.pop(context,true);}
        )),
        body: Container(
          child: Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height - 200,
                width: MediaQuery.of(context).size.width,
                child: GoogleMap(
                  onMapCreated: _onMapCreated,
                  initialCameraPosition:
                      CameraPosition(target: LatLng(0.0, 0.0), zoom: 11.0),
                  onTap: _onMapClicked,
                  markers: _markers,
                  onLongPress: _onMapClicked,
                ),
              ),
              Text(
                _placeAddress,
                style: TextStyle(fontSize: 17) ,
              ),
              FlatButton(
                child: Text("Next"),color: Color(0XFFEF6514),
                onPressed: () {

                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                      if(latitude==0.0 || longitude==0.0)
                      {
                        return AlertDialog(
                          title: Text("Message"),
                          content: Text("Please select the place of the toilet in google before proceed"),
                          actions: <Widget>[FlatButton(
                            child: Text("Okay"),
                            onPressed: ()
                            {
                              Navigator.of(context).pop();
                            },
                          )],
                        );
                      }
                        return WashRoomInfoDialog(latitude,longitude,_placeAddress);
                      });
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget isImageAvailable(int position)
  {
    debugPrint("check for image availability");
 if(_image[position] == null)
 {
   debugPrint('image not available');
  return  Text('No image');
 }else
 {
   debugPrint('image available @${position}');
   return Image.file(_image[position]);
 }
  }

  void cameraOption() async {
    final cameras = await availableCameras();

    final firstCamera = cameras.first;
  }

  void hygineChangedDropDownItem(String selectedHygineLevel) {
    setState(() {
      currentHygineStatus = selectedHygineLevel;
    });
  }

  void placesChangedDropDownItem(String selectedPlace) {
    setState(() {
      places = selectedPlace;
    });
  }

  void paymentChangedDropDownItem(String selectedPayment) {
    setState(() {
      currentPayment = selectedPayment;
    });
  }

  void disabledAccessChangedDropDownItem(String selecteddisabledAccess) {
    setState(() {
      currentDisabledAccess = selecteddisabledAccess;
    });
  }

  void genderChangedDropDownItem(String selectedGender) {
    setState(() {
      currentGender = selectedGender;
    });
  }

  void _onMapClicked(LatLng latLng) async{
    //moveToUserCurrentLocation();
    latitude= latLng.latitude;
    longitude= latLng.longitude;
    await Geolocator().placemarkFromCoordinates(latLng.latitude,latLng.longitude).then((geocoderAddress){
// debugPrint(geocoderAddress[0].country);
// //debugPrint(geocoderAddress[0].position);
// debugPrint(geocoderAddress[0].locality);
// debugPrint(geocoderAddress[0].administrativeArea);
// debugPrint(geocoderAddress[0].postalCode);
// debugPrint(geocoderAddress[0].name);
// //debugPrint(geocoderAddress[0].subAdministratieArea);
// debugPrint(geocoderAddress[0].isoCountryCode);
// debugPrint(geocoderAddress[0].subLocality);
// debugPrint(geocoderAddress[0].subThoroughfare);
// debugPrint(geocoderAddress[0].thoroughfare);

debugPrint("address ${geocoderAddress[0].name+","+geocoderAddress[0].thoroughfare+","+geocoderAddress[0].subLocality+","+geocoderAddress[0].locality+","+geocoderAddress[0].administrativeArea+","+geocoderAddress[0].postalCode+","+geocoderAddress[0].country}");
    setState(() {
      if (_markers != null) {
        _markers.clear();
        _markers.add(Marker(
            markerId: MarkerId(latLng.toString()),
            position: latLng,
            icon: BitmapDescriptor.defaultMarker));
//_showInfoDialog(this.context);

_placeAddress=geocoderAddress[0].name+","+geocoderAddress[0].thoroughfare+","+geocoderAddress[0].subLocality+","+geocoderAddress[0].locality+","+geocoderAddress[0].administrativeArea+","+geocoderAddress[0].postalCode+","+geocoderAddress[0].country;
      }
    });
    });

    debugPrint("total markers ${_markers.length}");
  }

  void moveToUserCurrentLocation() async {
    debugPrint("move to current location");
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    debugPrint("position ${position.latitude}");
    //Future<NearByWashRoomList> washRoomDetails=fetchPost(position.latitude,position.longitude);

    setState(() {
      debugPrint("move to current location state ${position.latitude}");

      LatLng currentLatLng = LatLng(position.latitude, position.longitude);
      mapController.moveCamera(CameraUpdate.newLatLngZoom(currentLatLng, 15));
      _markers.add(Marker(
          markerId: MarkerId(position.toString()),
          position: currentLatLng,
          icon: BitmapDescriptor.defaultMarker));
    });
  }
}
