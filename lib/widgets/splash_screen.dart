import 'package:flutter/material.dart';
import './home_screen.dart';
import 'dart:async';

class SplashScreenWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const twentyMillis = const Duration(milliseconds: 5000);
    new Timer(twentyMillis, () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => GoogleMapView()),
      );
    });
    return Container(
      color: Color(0XFF0B379E),
      child: Center(
        child: Image.asset('assets/splash_screen.PNG'),
      ),
    );
  }
}  
