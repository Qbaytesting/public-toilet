import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:signalstrength/widgets/Toilet_Info.dart';
import './add_new_washroom_details.dart';

import 'package:signalstrength/models.dart';

class WashRoomListWidget extends StatefulWidget{

WashRoomListWidgetState createState()=>WashRoomListWidgetState();
}

class WashRoomListWidgetState extends State<WashRoomListWidget>
{
  NearByWashRoomList washRoomList;

@override
  void initState() {
    // TODO: implement initState
     fetchNearByWashRoomDetails();
    super.initState();
  }
@override
  Widget build(BuildContext context) {
   
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("List of Public Toilets"),
          leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: (){Navigator.pop(context,true);}
        )),
      body: Container(
      child: ListView.builder(
        itemBuilder: (listContext,postion)
        {
          if(washRoomList != null)
          {
            return GestureDetector(
              child: Card(
              elevation:5,
                margin: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 10.0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0), ),
                child : Padding(
                  padding: const EdgeInsets.all(10.0),
                  child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("${washRoomList.washRoomDetails[postion].address}"),
                      Text("Distance : ${washRoomList.washRoomDetails[postion].distance}"" km"),
                      Text("Gender: ${washRoomList.washRoomDetails[postion].genders}"),
                      Text("Cost: ${washRoomList.washRoomDetails[postion].cost}")
                    ],
                  ) ,
                ),
            ),
            onTap: ()=>{
              // debugPrint("clicked"),
              Navigator.push(context,MaterialPageRoute(
                builder: (context) => toiletInfo(washRoomList.washRoomDetails[postion].genders,
              washRoomList.washRoomDetails[postion].higenic,
              washRoomList.washRoomDetails[postion].place,
              washRoomList.washRoomDetails[postion].disabledAccess,
              washRoomList.washRoomDetails[postion].rating,
              washRoomList.washRoomDetails[postion].address,
              washRoomList.washRoomDetails[postion].cost,
              washRoomList.washRoomDetails[postion].firstImageUrl,
              )
              ),)
            },
            );

          }
            
        },
        itemCount: (washRoomList != null && washRoomList.washRoomDetails != null)?washRoomList.washRoomDetails.length:0),
    ),
      ),
    );
  }

   Future<NearByWashRoomList> fetchNearByWashRoomDetails() async
  {
Position position = await Geolocator()
      .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

  Map<String, String> bodyData = Map();
  bodyData["Latitude"] = position.latitude.toString();
  bodyData["Longitude"] = position.longitude.toString();

  final response = await http.post(
      'http://quadrobay.co.in/Toilet_App/public/api/showroom/toilet-get-data',
      headers: {"Content-Type": "application/json"},
      body: json.encode(bodyData));

  debugPrint('body data ${bodyData.toString()}');
  if (response.statusCode == 200) {
    debugPrint("data loaded with 200Ok ${response.body}");
    //NearByWashRoomList washRoomDetails = NearByWashRoomList.fromJson(jsonDecode(response.body));
       setState(() {
       this.washRoomList=NearByWashRoomList.fromJson(jsonDecode(response.body)); 
    });
    return NearByWashRoomList.fromJson(jsonDecode(response.body));
  } else {
    debugPrint("failed to load");
    throw Exception('Failed to load post');
  }
  }
}