import 'package:flutter/material.dart';
import 'dart:async';
import 'package:camera/camera.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:signalstrength/widgets/home_screen.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

class WashRoomInfoDialog extends StatefulWidget {
  double latitude, longitude;
  String address;
  WashRoomInfoDialog(this.latitude, this.longitude, this.address) {}
  @override
  WashRoomInfoDialogState createState() =>
      WashRoomInfoDialogState(latitude, longitude, address);
}

class WashRoomInfoDialogState extends State<WashRoomInfoDialog> {
  double latitude, longitude;
  String address;
  double washRoomRating = 1;
  WashRoomInfoDialogState(this.latitude, this.longitude, this.address) {}

  String _placeAddress = "Ambathur,chennai,600089";
  List<String> genderList = ['Male', 'Female', 'Both'];
  List<String> hygineList = ['Clean', 'Good', 'Dirty'];
  List<String> placesList = [
    'Restaurant',
    'Shopping Center',
    'Public',
    'Gas Station'
  ];
  List<String> paymentDetails = ['Free', 'Pay'];
  List<String> disabledAccess = ['Yes', 'No'];
  String currentGender = 'Male',
      currentHygineStatus = 'Clean',
      places = 'Restaurant',
      currentPayment = "Free",
      currentDisabledAccess = 'Yes';
  List<DropdownMenuItem<String>> genderDropDown,
      hygineDropDown,
      placesDropDown,
      paymentDropDown,
      disableAccessDropDown;

  List<File> _image = new List(3);

  Future getImage(int position) async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      debugPrint("captured image details ${image} @ position ${position}");
      _image[position] = image;
    });
  }


  // --------------------------------------------



  // 2. compress file and get file.
  Future<File> testCompressAndGetFile(File file, String targetPath) async {
    var result = await FlutterImageCompress.compressAndGetFile(
        file.absolute.path, targetPath,
        quality: 88,
        rotate: 180,
      );

    print(file.lengthSync());
    print(result.lengthSync());

    return result;
  }



// -------------------------------------



  @override
  void initState() {
    debugPrint('washroom widget inits');
    genderDropDown = getGenderDropDownMenuItems(genderList);
    hygineDropDown = getGenderDropDownMenuItems(hygineList);
    placesDropDown = getGenderDropDownMenuItems(placesList);
    paymentDropDown = getGenderDropDownMenuItems(paymentDetails);
    disableAccessDropDown = getGenderDropDownMenuItems(disabledAccess);

    super.initState();
  }

  List<DropdownMenuItem<String>> getGenderDropDownMenuItems(List<String> data) {
    List<DropdownMenuItem<String>> items = new List();
    for (String d in data) {
      items.add(new DropdownMenuItem(value: d, child: new Text(d)));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Builder(
        builder: (BuildContext context)
        {
            return Column(
        children: <Widget>[
          Text(""),
          Text(
            "Washroom Info",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Text(""),
          Text("Select Gender"),
          DropdownButton(
            value: currentGender,
            items: genderDropDown,
            onChanged: genderChangedDropDownItem,
          ),
          Text("Select Place"),
          DropdownButton(
            value: places,
            items: placesDropDown,
            onChanged: placesChangedDropDownItem,
          ),
          Text("Is there any payment available?"),
          DropdownButton(
            value: currentPayment,
            items: paymentDropDown,
            onChanged: paymentChangedDropDownItem,
          ),
          Text("Does disabled able to use? "),
          DropdownButton(
            value: currentDisabledAccess,
            items: disableAccessDropDown,
            onChanged: disabledAccessChangedDropDownItem,
          ),
          Text("How much hygiene is it?"),
          DropdownButton(
              value: currentHygineStatus,
              items: hygineDropDown,
              onChanged: hygineChangedDropDownItem),
          Text("Ratings"),
          FlutterRatingBar(
              initialRating: washRoomRating,
              onRatingUpdate: (rating) {
                washRoomRating = rating;
              },
              fillColor: Colors.amber,
              borderColor: Colors.amber.withAlpha(50),
              allowHalfRating: false,
              itemSize: 21),
              Text(""),
          // Row(
          //   children: <Widget>[
          //     FlatButton(
          //       child: Container(
          //         child: isImageAvailable(0),
          //       ),
          //       onPressed: () {
          //         getImage(0);
          //       },
          //     ),
          //     FlatButton(
          //         child: isImageAvailable(1),
          //         onPressed: () {
          //           getImage(1);
          //         }
                                      // Navigator.pushReplacement(
                                      //   context,
                                      //   MaterialPageRoute(
                                      //       builder: (context) => CameraWidget(
                                      //             camera: CameraDescription(),
                                      //           )
                                      //           ),
                                      // );
                                    // },
          //         ),
          //     FlatButton(
          //         child: isImageAvailable(2),
          //         onPressed: () {
          //           getImage(2);
          //         }),
          //   ],
          // ),
          FlatButton(
            child: Text(
              "Save Details",
              style: TextStyle(fontSize: 18),
            ),
            color: Color(0XFFEF6514),
            onPressed: () {
              sendWashroomDetailsToServer(context);
            },
          )
        ],
      );
        },
      )
    );
    ;
  }

  Widget isImageAvailable(int position) {
    debugPrint("check for image availability");
    if (_image[position] == null) {
      debugPrint('image not available');
      return Text('No image');
    } else {
      debugPrint('image available @${position}');
      return Image.file(_image[position],
          fit: BoxFit.fill, height: 50, width: 50);
    }
  }

  void hygineChangedDropDownItem(String selectedHygineLevel) {
    setState(() {
      currentHygineStatus = selectedHygineLevel;
    });
  }

  void placesChangedDropDownItem(String selectedPlace) {
    setState(() {
      places = selectedPlace;
    });
  }

  void paymentChangedDropDownItem(String selectedPayment) {
    setState(() {
      currentPayment = selectedPayment;
    });
  }

  void disabledAccessChangedDropDownItem(String selecteddisabledAccess) {
    setState(() {
      currentDisabledAccess = selecteddisabledAccess;
    });
  }

  void genderChangedDropDownItem(String selectedGender) {
    setState(() {
      currentGender = selectedGender;
    });
  }

  void sendWashroomDetailsToServer(BuildContext context) async {
    var washRoomData = {
      "Latitude": latitude,
      "Longitude": longitude,
      "Gender": currentGender,
      "Higenic": currentHygineStatus,
      "Place": places,
      "Rating": washRoomRating,
      "Disabled_Access": currentDisabledAccess,
      "Cost": currentPayment,
      "Address": address,
      "Comment": "",
    };

    await http
        .post(
            "http://quadrobay.co.in/Toilet_App/public/api/showroom/toilet-project",
            headers: {"Content-Type": "application/json"},
            body: json.encode(washRoomData))
        .then((response) {
      debugPrint("response ${response.body}");
      if (response.statusCode == 200) {
        Map responseData = jsonDecode(response.body);
        List data = responseData['Response'];
        String id = data.elementAt(0)['Toilet_ID'];

        //data.elementAt(0);

        debugPrint(
            " response data ${responseData['Response']} element ${data.elementAt(0)} ${id}");
        // uploadImages(id,context);
        if(_image != null && _image.length>0)
            {
              uploadImages(id,context);
            }
      }
    });
  }

  void uploadImages(String toiletID,BuildContext context) async {
    //int length = await _image[0].length();

    var uri = Uri.parse(
        "http://quadrobay.co.in/Toilet_App/public/api/showroom/toilet-upload-images");

    var request = new http.MultipartRequest("POST", uri);

    for (int i = 0; i < _image.length; i++) {
      // var multipartFile = http.MultipartFile.fromBytes(

        if(_image[i] != null)
      {
        
      var multipartFile = http.MultipartFile.fromBytes("pic ${toiletID} ${i}.png", _image[i].readAsBytesSync());
      request.files.add(multipartFile);  
      }
      //     "pic ${toiletID} ${i}.png", _image[i].readAsBytesSync());
      // request.files.add(multipartFile);
    }
    //var multipartFile = new http.MultipartFile('pic', _image[0].readAsBytesSync(), length,);
    request.fields['Toilet_ID'] = toiletID;

    var response = await request.send();
    // if (response.statusCode == 200) {
      //final snackBar = SnackBar(content: Text('New toilet details are added successfully'));
      //Scaffold.of(context).showSnackBar(snackBar);
      // Navigator.of(context).pop();
      Navigator.push(context,MaterialPageRoute(builder: (context) => GoogleMapView()),);
    // }
    debugPrint("status code for image ${response.statusCode}");
  }
}
