// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NearByWashRoomList _$NearByWashRoomListFromJson(Map<String, dynamic> json) {
  return NearByWashRoomList((json['Response'] as List)
      ?.map((e) => e == null
          ? null
          : WashRoomPlaceDetails.fromJson(e as Map<String, dynamic>))
      ?.toList());
}

Map<String, dynamic> _$NearByWashRoomListToJson(NearByWashRoomList instance) =>
    <String, dynamic>{'Response': instance.washRoomDetails};

WashRoomPlaceDetails _$WashRoomPlaceDetailsFromJson(Map<String, dynamic> json) {
  return WashRoomPlaceDetails(
      json['Toilet_ID'] as String,
      json['users_lat'] as String,
      json['users_long'] as String,
      json['distance'] as String,
      json['Gender'] as String,
      json['Higenic'] as String,
      json['Place'] as String,
      json['Rating'] as String,
      json['Disabled_Access'] as String,
      json['Cost'] as String,
      json['Address'] as String,
      json['First_Image'] as String,
      json['Second_Image'] as String,
      json['Third_Image'] as String,
      (json['Comment'] as List)
          ?.map((e) => e == null
              ? null
              : WashRoomComments.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$WashRoomPlaceDetailsToJson(
        WashRoomPlaceDetails instance) =>
    <String, dynamic>{
      'Toilet_ID': instance.toiletID,
      'users_lat': instance.washRoomLat,
      'users_long': instance.washRoomLng,
      'distance': instance.distance,
      'Gender': instance.genders,
      'Higenic': instance.higenic,
      'Place': instance.place,
      'Rating': instance.rating,
      'Disabled_Access': instance.disabledAccess,
      'Cost': instance.cost,
      'Address': instance.address,
      'First_Image': instance.firstImageUrl,
      'Second_Image': instance.secondImageUrl,
      'Third_Image': instance.thirdImageUrl,
      'Comment': instance.commentsDetails
    };

WashRoomComments _$WashRoomCommentsFromJson(Map<String, dynamic> json) {
  return WashRoomComments(json['Command_ID'], json['Command'], json['Rating']);
}

Map<String, dynamic> _$WashRoomCommentsToJson(WashRoomComments instance) =>
    <String, dynamic>{
      'Command_ID': instance.commentID,
      'Command': instance.comment,
      'Rating': instance.rating
    };
