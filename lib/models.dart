import 'package:json_annotation/json_annotation.dart';

part 'models.g.dart';

@JsonSerializable()
class NearByWashRoomList{
  @JsonKey(name : "Response")
  List<WashRoomPlaceDetails> washRoomDetails;

  NearByWashRoomList(this.washRoomDetails);
  
  factory NearByWashRoomList.fromJson(Map<String, dynamic> data) {
    return _$NearByWashRoomListFromJson(data);
  }

}

@JsonSerializable()
class WashRoomPlaceDetails{
@JsonKey(name : "Toilet_ID")
String toiletID;
@JsonKey(name : "users_lat")
String washRoomLat;
@JsonKey(name : "users_long")
String washRoomLng;
@JsonKey(name : "distance")
String distance;
@JsonKey(name : "Gender")
String genders;
@JsonKey(name : "Higenic")
String higenic;
@JsonKey(name : "Place")
String place;
@JsonKey(name : "Rating")
String rating;
@JsonKey(name : "Disabled_Access")
String disabledAccess;
@JsonKey(name : "Cost")
String cost;
@JsonKey(name : "Address")
String address;
@JsonKey(name : "First_Image")
String firstImageUrl;
@JsonKey(name : "Second_Image")
String secondImageUrl;
@JsonKey(name : "Third_Image")
String thirdImageUrl;
@JsonKey(name : "Comment")
List<WashRoomComments> commentsDetails;

WashRoomPlaceDetails(this.toiletID,this.washRoomLat,this.washRoomLng,this.distance,this.genders,
this.higenic,this.place,this.rating,this.disabledAccess,this.cost,this.address,this.firstImageUrl,
this.secondImageUrl,this.thirdImageUrl,this.commentsDetails
);

factory WashRoomPlaceDetails.fromJson(Map<String, dynamic> data)=>_$WashRoomPlaceDetailsFromJson(data);


}

@JsonSerializable()
class WashRoomComments{
  @JsonKey(name : "Command_ID")
  String commentID;
  @JsonKey(name : "Command")
  String comment;
  @JsonKey(name : "Rating")
  String rating;

  WashRoomComments(commentID,comment,rating){

  }

  factory WashRoomComments.fromJson(Map<String, dynamic> data){
    return _$WashRoomCommentsFromJson(data);
  }
}