#include "AppDelegate.h"
#include "GeneratedPluginRegistrant.h"
#import "GoogleMaps/GoogleMaps.h"

// @implementation AppDelegate

// - (BOOL)application:(UIApplication *)application
//     didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//   [GeneratedPluginRegistrant registerWithRegistry:self];
//   // Override point for customization after application launch.
//   [GMSServices provideAPIKey: @"AIzaSyD2sWrtcXvhwVXCYKBM8MNYi6WG0KPZtI0"];
//   return [super application:application didFinishLaunchingWithOptions:launchOptions];
// }

// @end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [GMSServices provideAPIKey:@"AIzaSyB4gvmJBDB3x6w47lpsBWYm-evuDyJ_A-k"];
  [GeneratedPluginRegistrant registerWithRegistry:self];
  return [super application:application didFinishLaunchingWithOptions:launchOptions];
}
@end

// <meta-data android:name="com.google.android.geo.API_KEY" 
//                android:value="AIzaSyD2sWrtcXvhwVXCYKBM8MNYi6WG0KPZtI0"/>Rr